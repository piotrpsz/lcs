# LCS (Longest Common Subsequence) - diff

### Example: display differences between files as diff
```c++
Parser diff;

// when you would like only display
diff.compare_files(pathA, pathB);
cout << diff << '\n';

// when you would like to use result of parsing
auto result = diff.result();
```
as example you can see:
```c++
...
29a29,30
33c34
35,36d35
38a38,39
42c43
44,45d44
47a47,48
...
```