#include <iostream>
#include <bit>
#include "parser.h"

using namespace std;

int main() {
    cout << __cplusplus << "\n\n";

    string pathA{"/Users/piotr/test/test1.txt"};
    string pathB{"/Users/piotr/test/test2.txt"};

    Parser diff;

    // when you would like only display
    diff.compare_files(pathA, pathB);
    cout << diff << '\n';

    // when you would like to use result of parsing
    auto result = diff.result();

    return 0;
}
