//
// Created by Piotr Pszczółkowski on 15/03/2023.
//

#include "parser.h"
#include "segment.h"
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <tuple>

using namespace std;

/// Compare the contents of two specified files.
/// \param pathA path to 1st file
/// \param pathB path to the 2nd file
Parser Parser::compare_files(string const &pathA, string const &pathB) noexcept {
    if (auto contentA = read_file_to_string(pathA); contentA) {
        if (auto contentB = read_file_to_string(pathB); contentB) {
            boost::split(rowsA, *contentA, boost::is_any_of("\n"));
            boost::split(rowsB, *contentB, boost::is_any_of("\n"));
            segments = find_others_segments(find_same_segments(rowsA, rowsB));
        }
    }
    return *this;
}

/// Based on information about common segments, we determine information
/// about other segments (added, deleted, changed).
/// \param same vector with information about common segments.
/// \return vector with found segments (added, deleted, changed).
vector<Segment>
Parser::find_others_segments(vector<Segment> const &same) const noexcept {
    vector<Segment> others;
    others.reserve(1000);
    Segment prv;

    const auto dist = [](int const start, int const end) {
        return (end >= start) ? (end - start + 1) : 0;
    };

    for (int i = 0; i < same.size(); i++) {
        auto const &item = same[i];

        auto const startA = i ? (prv.first.end + 1) : 0;
        auto const endA = item.first.start - 1;
        auto const nA = dist(startA, endA);

        auto const startB = i ? (prv.second.end + 1) : 0;
        auto const endB = item.second.start - 1;
        auto const nB = dist(startB, endB);

        if (nA >= 0 || nB >= 0) {
            if (nA == 0 && nB > 0) {
                others.emplace_back(i ? Range{same[i - 1].first.end} : Range{},
                                    Range{startB, endB},
                                    Action::Added);
            } else if (nA > 0 && nB == 0) {
                others.emplace_back(Range(startA, endA),
                                    i ? Range{same[i - 1].second.end} : Range{},
                                    Action::Deleted);
            } else {
                others.emplace_back(Range{startA, endA},
                                    Range{startB, endB},
                                    Action::Changed);
            }
        }
        prv = item;
    }

    others.shrink_to_fit();
    return others;
}
