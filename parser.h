//
// Created by Piotr Pszczółkowski on 15/03/2023.
//
#ifndef LCS_PARSER_H
#define LCS_PARSER_H

#include "segment.h"
#include <fstream>
#include <iostream>
#include <optional>
#include <tuple>
#include <vector>

class Parser {
    using Vector2D = std::vector<std::vector<int>>;
    using Rows = std::vector<std::string>;
    using Segments = std::vector<Segment>;

    Rows rowsA;
    Rows rowsB;
    Segments segments;

public:
    Parser compare_files(std::string const& pathA, std::string const& pathB) noexcept;
    Segments result() const noexcept { return segments; }

private:
    /// Start searching for common line segments.
    /// The function ends the search after finding the 1st common lines
    /// and inserting the 1st segment into the 'segments' vector.
    /// The function is isolated so that the proper search does
    /// not have to constantly check if the vector is empty.
    /// \param lens vector-2d with the results of the LCS operation
    /// \param rowsA text lines A
    /// \param rowsB text lines B
    /// \return vector with the found segment and positions from
    /// which further searches should be continued (in both files)
    /// \warning it is c++20 template (call parameters as auto)
    [[nodiscard]] std::tuple<Segments, int, int>
    find_same_segments_start(Vector2D const &lens, auto const& itemsA, auto const& itemsB) const noexcept {
        Segments segments;
        segments.reserve(lens.size());
        auto i = int(rowsA.size()) - 1;
        auto j = int(rowsB.size()) - 1;

        while (i >= 0 && j >= 0) {
            const auto &s1 = rowsA[i];
            const auto &s2 = rowsB[j];
            if (s1 == s2) {
                segments.emplace_back(Range{i}, Range{j});
                --i;
                --j;
                // Mamy pierwszy segment w wektorze - wracamy.
                break;
            } else if (lens[i + 1][j] > lens[i][j + 1])
                --j;
            else
                --i;
        }

        return make_tuple(segments, i, j);
    }

    /// Finding common lines of text
    /// \param rowsA text lines A
    /// \param rowsB text lines B
    /// \return vector of common segments
    /// \warning it is c++20 template (call parameters as auto)
    [[nodiscard]] Segments
    find_same_segments(auto const& itemsA, auto const& itemsB) const noexcept {
        auto const lens = lcs(itemsA, itemsB);
        auto [segments, i, j] = find_same_segments_start(lens, itemsA, itemsB);

        while (i >= 0 && j >= 0) {
            auto const &s1 = rowsA[i];
            auto const &s2 = rowsB[j];
            if (s1 == s2) {
                // back zawsze istnieje
                Segment &s = segments.back();
                if (s.first.is_below(i) && s.second.is_below(j)) {
                    s.first.start = i;
                    s.second.start = j;
                } else {
                    segments.emplace_back(Range{i}, Range{j});
                }
                --i;
                --j;
            } else if (lens[i + 1][j] > lens[i][j + 1]) {
                --j;
            } else {
                --i;
            }
        }
        // The previous iteration was done backwards, we reverse the result.
        reverse(segments.begin(), segments.end());
        return segments;
    }


    [[nodiscard]]
    std::vector<Segment> find_others_segments(std::vector<Segment> const& same) const noexcept;



    /// Determination of common line information.
    /// \return array with common line markers.
    /// \warning it is c++20 template (call parameters as auto)
    [[nodiscard]]
    Vector2D lcs(auto const& itemsA, auto const& itemsB) const noexcept {
        auto const countA = itemsA.size();
        auto const countB = itemsB.size();

        Vector2D lens(countA + 1);
        std::for_each(lens.begin(), lens.end(), [countB](auto& vec) {
            vec = std::move(std::vector<int>(countB + 1));
        });

        for (auto i = 0; i < countA; i++)
            for (auto j = 0; j < countB; j++)
                lens[i + 1][j + 1] = (rowsA[i] == rowsB[j]) ? (1 + lens[i][j]) : std::max(lens[i + 1][j], lens[i][j + 1]);

        return lens;
    }

    /// Reads file's content to string
    /// \param fpath path to a file
    /// \return file's content as string
    ///
    static std::optional<std::string>
    read_file_to_string(std::string const& fpath) noexcept {
        try {
            std::ifstream ifs(fpath);
            return std::string(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>());
        } catch (std::ifstream::failure& err) {
            std::cerr << err.what() << '\n';
            return std::nullopt;
        }
    }

    /// Returns action's marker.
    /// \param action we need a marker for this action
    /// \return action's marker as string
    ///
    static std::string action_marker(const Action action) noexcept {
        switch (action) {
            case Action::Added:
                return "a";
            case Action::Deleted:
                return "d";
            case Action::Changed:
                return "c";
            case Action::Same:
                return "s";
            default:
                return "?";
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const Parser& p) {
        std::for_each(p.segments.begin(), p.segments.end(), [&](auto segment) {
            os << segment.first << action_marker(segment.action) << segment.second << '\n';
        });
        return os;
    }
};

#endif // LCS_PARSER_H
