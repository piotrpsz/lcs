//
// Created by Piotr Pszczółkowski on 14/03/2023.
//
#ifndef LCS_RANGE_H
#define LCS_RANGE_H

#include <string>
#include <sstream>

static const int NOT_USED = -1;

struct Range {
    int start = NOT_USED;
    int end = NOT_USED;

    explicit Range() = default;
    explicit Range(const int a) noexcept: start(a), end(a) {}
    explicit Range(const int a, const int b) noexcept: start(a), end(b) {}
    Range(const Range&) = default;
    Range(Range&&) = default;
    Range& operator=(const Range&) = default;
    Range& operator=(Range&&) = default;
    ~Range() = default;

    [[nodiscard]]
    bool valid() const noexcept {
        return (start != NOT_USED && end != NOT_USED);
    }

    [[nodiscard]]
    bool is_below(const size_t pos) const noexcept {
        return ((start - 1) == pos);
    }

    [[nodiscard]]
    std::string str() const noexcept {
        std::stringstream ss;

        ss << (start + 1);
        if (NOT_USED != end && end != start) {
            ss << ',' << (end + 1);
        }
        return ss.str();
    }

    friend std::ostream& operator<<(std::ostream& s, Range const& range) {
        s << range.str();
        return s;
    }
};

#endif //LCS_RANGE_H
