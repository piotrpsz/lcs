//
// Created by Piotr Pszczółkowski on 14/03/2023.
//
#ifndef LCS_SEGMENT_H
#define LCS_SEGMENT_H

#include <string>
#include <sstream>
#include "range.h"
#include "parser.h"

enum class Action {
    Same,
    Added,
    Deleted,
    Changed,
};

struct Segment {
    Range first;
    Range second;
    Action action;

    Segment() noexcept: first(), second(), action(Action::Same) {}
    Segment(Range a, Range b) noexcept: first(a), second(b) {}
    Segment(Range a, Range b, Action c) noexcept: first(a), second(b), action(c) {}
    Segment(const Segment &) = default;
    Segment(Segment &&) = default;
    Segment &operator=(const Segment &) = default;
    Segment &operator=(Segment &&) = default;
    ~Segment() = default;

    [[nodiscard]]
    bool valid() const noexcept {
        return (first.valid() && second.valid());
    }
};

#endif //LCS_SEGMENT_H
